<?php

use App\Http\Controllers\SurveyController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RespondentController;
use App\Http\Controllers\AnalyticsController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::get('fetch-me', [AuthController::class, 'fetch_me'])->name('fetch_me');
    Route::get('logout-user', [AuthController::class, 'logout_user'])->name('logout_user');
    Route::get('variants', [AuthController::class, 'variants'])->name('variants');

    Route::post('authenticate-user', [AuthController::class, 'authenticate_user'])->name('authenticate_user');
    Route::post('register-user', [AuthController::class, 'register_user'])->name('register_user');
});

Route::prefix('admin')->group(function () {
    Route::get('get-my-survey/{id}', [SurveyController::class, 'get_my_survey'])->name('get_my_survey');
    Route::get('close-my-survey/{id}', [SurveyController::class, 'close_my_survey'])->name('close_my_survey');
    Route::get('get-my-active-surveys', [SurveyController::class, 'get_my_active_surveys'])->name('get_my_active_surveys');
    Route::get('get-my-surveys', [SurveyController::class, 'get_my_surveys'])->name('get_my_surveys');
    Route::get('get-my-surveys-counter', [SurveyController::class, 'get_my_surveys_counter'])->name('get_my_surveys_counter');

    Route::post('define-survey', [SurveyController::class, 'define_survey'])->name('define_survey');
});

Route::prefix('respondent')->group(function () {
    Route::get('get-surveys', [RespondentController::class, 'get_surveys'])->name('get_surveys');
    Route::get('get-open-surveys', [RespondentController::class, 'get_open_surveys'])->name('get_open_surveys');
    Route::get('get-closed-surveys', [RespondentController::class, 'get_closed_surveys'])->name('get_closed_surveys');
    Route::get('get-open-survey/{id}', [RespondentController::class, 'get_open_survey'])->name('get_open_survey');
    Route::get('get-closed-survey/{id}', [RespondentController::class, 'get_closed_survey'])->name('get_closed_survey');
    Route::get('can-take-survey/{id}', [RespondentController::class, 'can_take_survey'])->name('can_take_survey');

    Route::post('complete-survey', [RespondentController::class, 'complete_survey'])->name('complete_survey');
});

Route::prefix('analytics')->group(function () {
    Route::get('get-survey/{id}', [AnalyticsController::class, 'get_survey'])->name('get_survey');
});