FROM tangramor/nginx-php8-fpm

# copy source code
COPY . /var/www/html

# start.sh will create laravel storage folder structure if $CREATE_LARAVEL_STORAGE = 1
ENV CREATE_LARAVEL_STORAGE "1"

ENTRYPOINT ["/bin/bash", "docker/start.sh" ]
