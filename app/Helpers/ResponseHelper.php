<?php

namespace App\Helpers;

class ResponseHelper
{
    protected $message;
    protected $status;
    protected $data;
    protected $status_code;

    public function __construct()
    {
        $this->data = null;
        $this->message = null;
        $this->status = false;
        $this->status_code = 400;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setStatus($status = false)
    {
        $this->status = $status;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setStatusCode(int $status_code)
    {
        $this->status_code = $status_code;
    }

    public function getResponse()
    {
        return [
            'status' => $this->status,
            'data' => $this->data,
            'message' => $this->message,
        ];
    }

    public function getStatusCode()
    {
        return $this->status_code;
    }

    public function setRawData($data)
    {
        $this->data = new \stdClass();
        $this->data->rawData = json_decode(json_encode($data[0]));
    }

    public function setTabularData()
    {
        $rawData = $this->data->rawData;
        $tableData = [];
        foreach ($rawData->questions as $question){
            $theQuestion = new \stdClass();
            $theQuestion->title = $question->text;
            $theQuestion->options = new \stdClass();
            foreach ($question->options as $key => $option){
                $n = $key+1;
                $text = "options".$n;
                $theQuestion->options->{$text} = count($option->respondents);
            }
            $tableData[] = $theQuestion;
        }
        $this->data->tableData = $tableData;
    }

    public function setPieData()
    {
        $rawData = $this->data->rawData;
        $male = $female = $unspecified = 0;
        foreach ($rawData->questions as $question){
            foreach ($question->options as $option){
                foreach ($option->respondents as $respondentData){
                    if (!empty($respondentData->respondent->gender)) {
                        switch ($respondentData->respondent->gender) {
                            case "m":
                                $male++;
                                break;
                            case "f":
                                $female++;
                                break;
                            default:
                                $unspecified++;
                        }
                    }
                }
            }
        }
        $this->data->pieData = [
            (object)["name" => "Male", "value"=> $male],
            (object)["name" => "Female", "value" => $female],
            (object)["name" => "Unspecified", "value" => $unspecified]
        ];
    }

    public function setBarData()
    {
        $rawData = $this->data->rawData;
        $barData = [];
        foreach ($rawData->questions as $question){
            $theQuestion = new \stdClass();
            $theQuestion->name = $question->text;
            foreach ($question->options as $key => $option){
                $n = $key+1;
                $text = "options".$n;
                $theQuestion->{$text} = count($option->respondents);
            }
            $barData[] = $theQuestion;
        }
        $this->data->barData = $barData;
    }
}
