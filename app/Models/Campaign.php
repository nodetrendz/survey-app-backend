<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Campaign extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'status'];

    public function poster()
    {
        return $this->belongsTo(User::class, 'poster_id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function campaign_results()
    {
        return $this->hasMany(CampaignResult::class);
    }

    /**
     * Method to handle the completion of a survey
     * 
     * @param array $parameters
     */
    public function complete_campaign(array $parameters)
    {
        $done = false;
        try {
            $campaign = Campaign::where(['id' => $parameters['survey']])->first();

            collect($parameters['answers'])->map(function ($ans) use ($campaign) {
                $question = Question::where(['id' => $ans['question']])->first();
                $option = $question->options()->where(['id' => $ans['option']])->first();

                $campaign_result = new CampaignResult();
                $campaign_result->campaign()->associate($campaign);
                $campaign_result->question()->associate($question);
                $campaign_result->selected_option()->associate($option);
                $campaign_result->respondent()->associate(Auth::user());
                $campaign_result->save();
            });
            $done = true;
        } catch (Exception $execption) {
            $done = $execption->getMessage();
        }
        return $done;
    }

    /**
     * Method to fetch the campaign by id or status.
     * If the id is null then the method returns campaigns
     * that are active.
     * 
     * @param boolean $is_active - (default: null).
     * @param int $id - (default : null).
     */
    public function get_campaign_by_id_or_status($is_active = null, $id = null)
    {
        $campaigns = null;

        if ($id === null && $is_active === null) {
            $campaigns = Campaign::get();
        } else if ($id === null && $is_active !== null) {
            $campaigns = Campaign::where(['status' => $is_active])->get();
        } else if ($id !== null && $is_active == null) {
            $campaigns = Campaign::where(['id' => $id])->get();
        } else if ($id !== null && $is_active !== null) {
            $campaigns = Campaign::where(['status' => $is_active, 'id' => $id])->first();
        }

        return $campaigns;
    }

    /**
     * Method to close a campaign / survey via id by a cordinator.
     * 
     * @param int $id - Campaign id.
     * 
     * @return boolean response - if the campaign was closed.
     */
    public function close(int $id)
    {
        try {
            $campaign = Campaign::find($id);
            $campaign->status = false;
            $campaign->save();
            return true;
        } catch (Exception $execption) {
            return false;
        }
    }

}
