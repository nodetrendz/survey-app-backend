<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'other_names',
        'last_name',
        'gender',
        'age',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * JWT tokens
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class, 'poster_id');
    }

    public function variant()
    {
        return $this->belongsTo(Variant::class);
    }

    public function campaign_results()
    {
        return $this->hasMany(CampaignResult::class, 'respondent_id');
    }

    public static function cordinator_counter($user)
    {
        $open_count = $user->campaigns()->where(['status' => true]);
        $closed_count = $user->campaigns()->where(['status' => false]);

        $total = $open_count->count() + $closed_count->count();
        if ($total <= 0) {
            $total = 1;
        }

        $open_surveys_count = $open_count->count();
        $open_surveys_percentage = $open_surveys_count / $total;

        $closed_surveys_count = $closed_count->count();
        $closed_surveys_percentage = $closed_surveys_count / $total;

        return [
            'open_surveys' => [
                'percentage' => number_format($open_surveys_percentage, 2) * 100,
                'score' => $open_surveys_count
            ],
            'closed_surveys' => [
                'percentage' => number_format($closed_surveys_percentage, 2) * 100,
                'score' => $closed_surveys_count
            ],
        ];
    }
}
