<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    protected $fillable = ['value'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function campaign_results()
    {
        return $this->hasMany(CampaignResult::class, 'option_id', 'id');
    }
}
