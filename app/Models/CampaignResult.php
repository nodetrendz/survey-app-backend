<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignResult extends Model
{
    use HasFactory;

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function selected_option()
    {
        return $this->belongsTo(Option::class, 'option_id');
    }

    public function respondent()
    {
        return $this->belongsTo(User::class, 'respondent_id');
    }
}
