<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Models\Variant;
use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Api Route: auth/authenticate-user
     * Method to handle user authentication.
     * 
     * @param Request $request
     * 
     * @return Response $response 
     */
    public function authenticate_user(Request $request)
    {
        $response = new ResponseHelper();
        $response->setStatusCode(401);

        $validator = Validator::make($request->only('username', 'password'), [
            'username' => ['required', 'email',],
            'password' => ['required', 'string', 'min:10',],
        ]);

        if ($validator->fails()) {
            $response->setMessage($validator->errors());
            $response->setStatusCode(400);
        } else {
            $credentials = ['email' => $request->username, 'password' => $request->password];

            try {
                if (!$token = Auth::attempt($credentials)) {
                    $response->setMessage("Invalid Credentials Entered!");
                } else {
                    $response->setStatusCode(200);
                    $response->setStatus(true);
                    $response->setMessage("Successfully Authenticated!");
                    $response->setData($token);
                }
            } catch (Exception $exception) {
                $response->setMessage("Unable to create token");
            }
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: auth/register-user
     * Method to handle user registration.
     * 
     * @param Request $request
     * 
     * @return Response $response
     */
    public function register_user(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string',],
            'other_names' => ['required', 'string',],
            'email' => ['required', 'email', 'unique:users',],
            'password' => ['required', 'min:10',],
            'gender' => ['required',],
            'age' => ['required',],
            'variant' => ['required']
        ]);

        $response = new ResponseHelper();
        if ($validator->fails()) {
            $response->setStatusCode(400);
            $response->setMessage($validator->errors());
        } else {
            try {
                $variant = Variant::where(['id' => $request->variant])->first();
                $user = null;

                if ($variant) {
                    $user = new User();
                    $user->first_name = $request->first_name;
                    $user->other_names = $request->other_names;
                    $user->email = $request->email;
                    $user->gender = $request->gender;
                    $user->password = Hash::make($request->password);
                    $user->age = $request->age;
                    $variant->users()->save($user);
                    $user = new UserResource($user);
                }
                $response->setStatus(true);
                $response->setStatusCode(201);
                $response->setData($user);
                $response->setMessage('Successfully created!');
            } catch (Exception $exception) {
                $response->setMessage($exception->getMessage());
                $response->setStatusCode(400);
            }
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: auth/logout-user
     * Method to destroy the user session
     * 
     * @param Request $request
     * 
     * @return Response $response
     */
    public function logout_user(Request $request)
    {
        $response = new ResponseHelper();
        try {
            $authorization = collect(explode('Bearer ', $request->header('Authorization')))->last();
            if ($authorization) {
                Auth::logout();
                $response->setStatus(true);
                $response->setStatusCode(200);
                $response->setMessage('Successfully logged out!');
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: auth/fetch-me
     * Method to get the logged in user
     * 
     * @param Request $request
     * 
     * @return Response $response
     */
    public function fetch_me(Request $request)
    {
        $response = new ResponseHelper();
        try {
            $person = new UserResource(Auth::user());
            if ($person) {
                $response->setStatus(true);
                $response->setStatusCode(200);
                $response->setMessage('Successfull!');
                $response->setData($person);
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }


    /**
     * Api Route: auth/variants
     * Method to get the variants
     * 
     * @param Request $request
     * 
     * @return Response $response
     */
    public function variants(Request $request)
    {
        $response = new ResponseHelper();
        try {
            $variants = Variant::all();
            if ($variants) {
                $response->setStatus(true);
                $response->setStatusCode(200);
                $response->setMessage('Successfull!');
                $response->setData($variants);
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }
}
