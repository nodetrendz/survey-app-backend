<?php

namespace App\Http\Controllers;

use Exception;

use App\Helpers\ResponseHelper;
use App\Http\Resources\CampaignResource;
use App\Http\Resources\RespondentSurveyResource;
use App\Models\Campaign;
use App\Models\CampaignResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RespondentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Method to get a campaign via status & or id
     * 
     * @param boolean $is_active
     * 
     * @return Response $response
     */
    private function get_campaign($is_active = null, $id = null)
    {
        $response = new ResponseHelper();

        try {
            $campaigns = (new Campaign())->get_campaign_by_id_or_status($is_active, $id);

            if ($id) {
                $response->setData(new CampaignResource($campaigns));
            } else {
                $response->setData(CampaignResource::collection($campaigns));
            }
            $response->setStatus(true);
            $response->setStatusCode(200);

            if (!$campaigns) {
                $response->setMessage("No record(s) Found!");
            } else {
                $response->setMessage("Record(s) Found Sucessfully!");
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: respondent/get-surveys
     * Method to fetch all surveys.
     * 
     * @param Request $request
     * 
     * @return Response $response - [Campaign]
     */
    public function get_surveys(Request $request)
    {
        $response = new ResponseHelper();

        try {
            $campaigns = (new Campaign())->get_campaign_by_id_or_status();
            $response->setData(RespondentSurveyResource::collection($campaigns));
            $response->setStatus(true);
            $response->setStatusCode(200);

            if (!$campaigns) {
                $response->setMessage("No records Found!");
            } else {
                $response->setMessage("Records Found Sucessfully!");
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: respondent/get-open-surveys
     * Method to fetch all open surveys.
     * 
     * @param Request $request
     * 
     * @return Response $response - [Campaign]
     */
    public function get_open_surveys(Request $request)
    {
        return $this->get_campaign(true);
    }

    /**
     * Api Route: respondent/get-open-surveys
     * Method to fetch all closed surveys.
     * 
     * @param Request $request
     * 
     * @return Response $response - [Campaign]
     */
    public function get_closed_surveys(Request $request)
    {
        return $this->get_campaign(false);
    }

    /**
     * Api Route: respondent/get-open-surveys
     * Method to fetch an open survey via it's id.
     * 
     * @param Request $request
     * @param int $id - campaign id
     * 
     * @return Response $response - Campaign
     */
    public function get_open_survey(Request $request, $id)
    {
        return $this->get_campaign(true, $id);
    }

    /**
     * Api Route: respondent/get-open-surveys
     * Method to fetch a closed survey via it's id.
     * 
     * @param Request $request
     * @param int $id - campaign id
     * 
     * @return Response $response
     */
    public function get_closed_survey(Request $request, $id)
    {
        return $this->get_campaign(false, $id);
    }

    /**
     * Api Route: respondent/complete-survey
     * method to complete a survey
     * 
     * @param Request $request
     * 
     * @return Response $response 
     */
    public function complete_survey(Request $request)
    {
        $response = new ResponseHelper();
        $validator = Validator::make($request->all(), [
            'survey' => ['required',],
            'answers' => ['required',],
        ]);

        if ($validator->fails()) {
            $response->setMessage($validator->errors());
        } else {
            $saved = (new Campaign())->complete_campaign($request->all());

            if ($saved === true) {
                $response->setStatus(true);
                $response->setStatusCode(200);
                $response->setMessage("Successfully Completed Survey!");
            } else {
                $response->setMessage($saved);
            }
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }


    /**
     * Api Route: respondent/can-take-survey/id
     * method to confirm if a customer can take a survey by id
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return Response $response 
     */
    public function can_take_survey(Request $request, int $id)
    {
        $response = new ResponseHelper();
        $respondent = Auth::user();

        $campaigns = CampaignResult::where([ 'campaign_id' => $id, 'respondent_id' => $respondent->id])->count();

        if ($campaigns > 0) {
            $response->setStatusCode(200);
            $response->setMessage("Respondent cannot perform survey!");
        } else {
            $response->setStatus(true);
            $response->setStatusCode(200);
            $response->setMessage("Respondent can perform survey!");
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }
}
