<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Resources\SurveyCounterResource;
use App\Http\Resources\SurveyListingResource;
use App\Models\Campaign;
use App\Models\Option;
use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use JWTAuth;
use Exception;

class SurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Api route: admin/define-survey
     * method to create a survey posted by a survey coordinator.
     * 
     * @param Request $request
     * 
     * @return Response $response 
     */
    public function define_survey(Request $request)
    {
        $response = new ResponseHelper();
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string'],
            'questions' => ['required'],
        ]);

        if ($validator->fails()) {
            $response->setMessage($validator->errors());
        } else {
            $user = Auth::user();

            if ($user) {
                $campaign = new Campaign();
                $campaign->title = $request->title;
                $campaign->status = true;
                $campaign->poster()->associate(Auth::user());
                $campaign->save();

                collect($request->questions)->each(function ($item) use ($campaign) {
                    $question = new Question();
                    $question->text = $item['text'];

                    $campaign->questions()->save($question);

                    collect($item['options'])->each(function ($opt) use ($question) {
                        $option = new Option();
                        $option->value = $opt;
                        $question->options()->save($option);
                    });
                });

                $response->setData($campaign);
                $response->setMessage('Created Successfully!');
                $response->setStatus(true);
                $response->setStatusCode(201);
            } else {
                $response->setMessage('Unauthenticated user');
                $response->setStatus(401);
            }
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api route: admin/get-my-survey
     * method to get a survey by id posted by the authenticated survey coordinator.
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return Response $response
     */
    public function get_my_survey(Request $request, int $id)
    {
        $response = new ResponseHelper();
        try {
            $campaign = Campaign::find($id);
            $response->setStatusCode(200);
            $response->setStatus(true);

            if ($campaign) {
                $response->setMessage('Successfully Found!');
                $response->setData($campaign);
            } else {
                $response->setMessage('No Records found!');
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }
        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: admin/get-my-active-surveys
     * method to retrieve all the active surveys for a survey coordinator.
     * 
     * @param Request $request
     * 
     * @return Response $response
     */
    public function get_my_active_surveys(Request $request)
    {
        $response = new ResponseHelper();
        try {
            $poster = Auth::user();

            $response->setStatusCode(200);
            if ($poster) {
                $response->setMessage("Records Found Successfully!");
                $response->setData($poster->campaigns);
                $response->setStatus(true);
            } else {
                $response->setMessage("No Records!");
                $response->setData([]);
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: admin/close-my-survey/{id}
     * method to close an active survey by id for a survey coordinator.
     * 
     * @param Request $request
     * @param int $id - survey id.
     * 
     * @return Response $response
     */
    public function close_my_survey(Request $request, int $id)
    {
        $response = new ResponseHelper();
        try {
            $survey = (new Campaign())->close($id);
            if ($survey) {
                $response->setStatus(true);
                $response->setMessage("Survey Closed Successfully!");
                $response->setStatusCode(200);
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: admin/get-my-surveys
     * method to close all surveys for a coordinator.
     * 
     * @param Request $request
     * 
     * @return Response $response
     */
    public function get_my_surveys(Request $request)
    {
        $response = new ResponseHelper();
        try {
            $user = Auth::user();

            if ($user) {
                $response->setStatus(true);
                $response->setMessage("Records Found Successfully!");
                $response->setStatusCode(200);

                $campaigns = SurveyListingResource::collection($user->campaigns);
                $response->setData($campaigns);
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

    /**
     * Api Route: admin/get-my-surveys-counter
     * method to close an active survey by id for a survey coordinator.
     * 
     * @param Request $request
     * 
     * @return Response $response
     */
    public function get_my_surveys_counter(Request $request)
    {
        $response = new ResponseHelper();
        try {
            $user = Auth::user();

            if ($user) {
                $response->setStatus(true);
                $response->setMessage("Records Found Successfully!");
                $response->setStatusCode(200);

                $counters = User::cordinator_counter($user);
                $response->setData($counters);
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }
}
