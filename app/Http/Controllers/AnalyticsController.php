<?php

namespace App\Http\Controllers;

use Exception;

use App\Helpers\ResponseHelper;
use App\Http\Resources\AnalyticsResource;
use App\Models\Campaign;
use Illuminate\Http\Request;

class AnalyticsController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Api Route: respondent/get-surveys
     * Method to fetch all surveys.
     *
     * @param Request $request
     *
     * @return Response $response - [Campaign]
     */
    public function get_survey(Request $request, $id = null)
    {
        $response = new ResponseHelper();

        try {
            $campaigns = (new Campaign())->get_campaign_by_id_or_status(null, $id);
            $response->setRawData(AnalyticsResource::collection($campaigns));
            $response->setTabularData();
            $response->setPieData();
            $response->setBarData();
            $response->setStatus(true);
            $response->setStatusCode(200);

            if (!$campaigns) {
                $response->setMessage("No records Found!");
            } else {
                $response->setMessage("Records Found Sucessfully!");
            }
        } catch (Exception $exception) {
            $response->setMessage($exception->getMessage());
        }

        return response()->json($response->getResponse(), $response->getStatusCode());
    }

}
