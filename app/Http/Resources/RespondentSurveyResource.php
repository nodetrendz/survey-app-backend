<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;

use Illuminate\Http\Resources\Json\JsonResource;

class RespondentSurveyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => Str::limit(Str::ucfirst($this->title), 15),
            'created' => $this->created_at->format('m/d/Y'),
            'status' => $this->status ? 'open' : 'close',
        ];
    }
}
