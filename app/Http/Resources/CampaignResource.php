<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => $this->status ? 'open' : 'close',
            'poster' => new UserResource($this->poster),
            'questions' => QuestionResource::collection($this->questions),
            'created_at' => $this->created_at->format('m/d/Y')
        ];
    }
}
