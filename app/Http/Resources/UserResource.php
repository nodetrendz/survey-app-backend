<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'other_names' => $this->other_names,
            'gender' => $this->gender,
            'age' => $this->age,
            'email' => $this->email,
            'variant' => [
                'id' => $this->variant->id,
                'name' => $this->variant->name,
            ],
            'registration_date' => $this->created_at,
        ];
    }
}
