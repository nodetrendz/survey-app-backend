<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SurveyListingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'survey_name' => $this->title,
            'date_created' => $this->created_at->format('m/d/Y'),
            'date_modified' => $this->updated_at->format('m/d/Y'),
            'questions' => $this->questions->count(),
            'responses' => $this->campaign_results->count(),
            'status' => $this->status ? 'open' : 'close',
        ];
    }
}
