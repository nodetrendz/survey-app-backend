#!/bin/sh

cd -P -- "$(dirname -- "$0")"/..

if [ "$1" == "production" ]; then
    docker build -t gcr.io/nodetrendz/survey-app-backend:latest -f Dockerfile.k8s . && \
    gcloud docker -- push gcr.io/nodetrendz/survey-app-backend:latest

    gcloud container clusters get-credentials $1 --zone europe-west2-a --project nodetrendz
    kubectl delete -f k8s/$1/deployment.yaml && \
    kubectl apply -f k8s/$1/deployment.yaml
else
    echo "argument error"
fi
