<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('variants')->insert([
            ['id' => 1, 'name' => 'Coordinator', ],
            ['id' => 2, 'name' => 'Respondent', ],
        ]);
    }
}
